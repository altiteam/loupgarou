var loup = angular.module('loup', ['ngRoute']);
var socket = io.connect('http://localhost:8888');

/****** variables *******/
/*
loup.filter('range', function() {
    return function(val, range) {
        range = parseInt(range);
        for (var i=0; i<range; i++)
            val.push(i);
        return val;
    };
});
*/


/******* SERVICE *******/
/*
 * #### service est global
 */
loup.service("gameService",
    function() {
        this.optionGame = {
            minPlayers : 9,
            players : [],
            roles : [
                "villagois"
                ,"La petite fille"
                ,"La Voyante"
                ,"La Sorcière"
                ,"Cupidon"
                ,"Le chasseur"
                ,"Le voleur"
                ,"Le salvateur"
                ,"L'idiot "
                ,"Le bouc émissaire"
                ,"L'ancien"
                ,"Le joueur de flûte"
                ,"Le Loup-garou blanc"
                ,"L'enfant sauvage"
                ,"Le renard"
                ,"La servante dévouée"
                ,"Les 3 frères "
                ,"montreur d'ours"
                ,"Le comédien"
                ,"Chevalier à l'épée rouillée"
                ,"Le juge bègue"
                ,"L'ange"
                ,"L'abominable sectaire"
                ,"L'infect père des loups"
                ,"Le chien-loup"
                ,"Le villageois-villageois"
                ,"Le grand méchant loup"
                ,"La gitane (sans philtre)"
                ,"Le garde champêtre"
                ,"Le chaman"
            ],
            gameMaster : false,
            initGame : false,
            mySession : null
        };

    }
);

/****** main page layout *************/
// controller principal
loup.controller('mainCtrl', ['$scope', '$location', 'gameService','$rootScope', function($scopeLayout, $location, gameService, $rootScope) {
    $scopeLayout.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };

    $scopeLayout.initGame = false;

    // a l'ecoute si un maitre du jeu est defini
    socket.on('gameMasterServer', function(data) {
        gameService.optionGame.gameMaster = data;
        $scopeLayout.initGame = data;
        $scopeLayout.$apply();
    });

    // a l'ecoute si un joueur rejoint la partie
    socket.on('addPlayerServer', function(data) {
        gameService.optionGame.players.push(data);
        $scopeLayout.$apply();

        console.log(gameService.optionGame.players);
    });
}]);


/****** index page ************/
loup.controller('indexCtrl', ['$scope','gameService', function($scope, gameService) {
    //$scope.initGame = gameService.optionGame.initGame;
}]);


/****** player inscription ************/
loup.controller('meetRoom', ['$scope','gameService', function($scope, gameService) {
    $scope.myPseudo = null;

    $scope.meetRoomFunc = function() {
        if($scope.myPseudo !== "") {
            gameService.optionGame.mySession = $scope.myPseudo;
            socket.emit('addPlayer',$scope.myPseudo);
        }
    }

}]);







/****** configure game *************/
loup.controller('configGameCtrl', ['$scope', 'gameService', function($scope, gameService ) {
    //socket.emit('initGame', { initGame: true });

    $scope.initGame = false;
    $scope.nbPlayer = 0;
    $scope.listPlayers = gameService.optionGame.players;

    socket.on('nb_users', function(data) {
        $scope.guest = gameService.optionGame.minPlayers - data;
        $scope.nbPlayer = data - 1;
        $scope.$apply();
    });

    $scope.initGameFunc = function() {
        $scope.initGame = true;
        gameService.optionGame.initGame = true;
        gameService.optionGame.gameMaster = true;

        console.log('la')
        // creation session gameMaster
        if(typeof(Storage) !== "undefined") {
            localStorage.setItem("pseudo", "gameMaster");
            gameService.optionGame.mySession = localStorage.getItem("pseudo");

            socket.emit('gameMasterDefined',true);
        } else {
            alert('votre navigateur ne supporte pas les webstorage');
        }

        // lancement de la partie
    }
}]);

/**** routes *****/
loup.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl : 'views/home.html'
        })
        .when('/serverList', {
            templateUrl : '/views/serverList.html'
        })
        .when('/configure', {
            templateUrl : '/views/viewMaster/configureMaster.html'
        })

});
















