# README #

### Gulp ###
Gulp est un *task runner* permettant d'automatiser les tâches de développement récurrentes.
Pour le projet, il nous permet de compiler/minifier/concaténer le LESS, le tout rapidement ! 
L'optimisation des images, du javascript peut être également utilisé.

### Installer Gulp & ses packages ###
En ligne de commande : 

- installer nodejs : npm install nodejs
- tapé : npm install

Pour Linux : 
Il est peut être necessaire d'installer node legacy :
npm install node-legacy

### Pour installer de nouveaux package gulp ###
utiliser la commande : 
npm install gulp-[nomPackage] --save-dev

### Lancer ###
Par defaut, lancer la commande :
Gulp

### Lancer le serveur ###
Utilisez la commande :
node server.js

Ouvrez la page avec l'adresse suivante : 
localhost:8888 
