var gulp = require('gulp'),
    less = require('gulp-less'),
    autoprefixer = require("gulp-autoprefixer"),
    gutil = require('gulp-util');

gulp.task('styles', function() {
    var l = less({});
    return gulp.src('public/css/less/*.less')
        .pipe(l)
        .on('error',function(e){
            gutil.log(e);
            this.emit('end');
        })
        .pipe(autoprefixer("last 2 versions", "> 1%", "Explorer8", "Android 2"))
        .pipe(gulp.dest('public/css'));
});
/*
gulp.task('glyphicons', function() {
    return gulp.src('img/icon/svg/*') // emplacement des icônes en SVG
        .pipe(iconfontCss({
            fontName: 'icons', // nom de l’iconfont
            targetPath: '../less/import/_fonts.less', // emplacement de la CSS finale
            fontPath: 'css/fonts/' // emplacement de la font finale
        }))
        .pipe(iconfont({
            fontName: 'iconsFont', // nom de l’iconfont
            normalize: true // si vos icônes n’ont pas la même taille, redimensionnement en prenant la taille la plus grande
        }))
        .pipe( gulp.dest('css/fonts') )
}) ;
*/

gulp.task('default', function() {
    gulp.watch(['public/css/less/**/*.less'], ['styles']);
});

