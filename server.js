var express = require('express');
    app = express();
    port = 8888;
    clc = require('cli-color');
    server = app.listen(port, function() {
        console.log("server started on http://localhost:"+clc.yellow(port));
    });
var io = require('socket.io')(server);

app.use(express.static(__dirname + '/public/'));



var users = 0,
    players = 0;

io.sockets.on('connection', function (socket) {
    //console.log(clc.green('Connection webSocket OK'));

    //ajoute un guest
    users++;
    console.log(users+' joueurs connecté \n ------------');

    // on enregistre le nombre d'invite sur la page
    io.sockets.emit('nb_users',users);
    // cote client si guest > 7 alors on affiche be bouton creation partie


    socket.on('initGame', function(data) {
        console.log('initGame send to server \n ------------');

        io.sockets.emit("gameStarted",data);
        if(data.initGame) {
            console.log('Game is started');
        }
    });

    socket.on('addPlayer', function(namePlayer) {
        console.log('Joueur ajouté : '+clc.blue(namePlayer)+ '\n ------------');
        io.sockets.emit("addPlayerServer",namePlayer);
    });



    socket.on('gameMasterDefined', function(data) {
        console.log(clc.green('Maitre du jeu défini \n ------------'));

        io.sockets.emit("gameMasterServer",data);
    });

    socket.on('disconnect', function() {
        users--;
        io.sockets.emit('nb_users',users);
        console.log(clc.red('joueur deconnecté \n ------------'));
    });
});